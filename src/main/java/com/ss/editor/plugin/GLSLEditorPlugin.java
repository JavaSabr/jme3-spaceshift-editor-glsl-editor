package com.ss.editor.plugin;

import com.ss.editor.annotation.FXThread;
import com.ss.editor.plugin.editor.RealTimeGLSLFileEditor;
import com.ss.editor.ui.component.editor.EditorRegistry;
import com.ss.rlib.plugin.PluginContainer;
import com.ss.rlib.plugin.PluginSystem;
import com.ss.rlib.plugin.annotation.PluginDescription;
import org.jetbrains.annotations.NotNull;

/**
 * The GLSL Editor plugin.
 *
 * @author JavaSaBr
 */
@PluginDescription(id = "com.ss.editor.GLSLEditor", version = "0.1")
public class GLSLEditorPlugin extends EditorPlugin {

    public GLSLEditorPlugin(@NotNull final PluginContainer pluginContainer) {
        super(pluginContainer);
    }

    @FXThread
    @Override
    public void onAfterCreateJavaFXContext(@NotNull final PluginSystem pluginSystem) {
        super.onAfterCreateJavaFXContext(pluginSystem);
        final EditorRegistry editorRegistry = EditorRegistry.getInstance();
        editorRegistry.addDescription(RealTimeGLSLFileEditor.DESCRIPTION);
    }
}

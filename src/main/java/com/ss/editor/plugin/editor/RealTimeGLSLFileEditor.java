package com.ss.editor.plugin.editor;

import com.ss.editor.FileExtensions;
import com.ss.editor.annotation.FromAnyThread;
import com.ss.editor.ui.component.editor.EditorDescription;
import com.ss.editor.ui.component.editor.impl.AbstractFileEditor;
import javafx.scene.layout.HBox;
import org.jetbrains.annotations.NotNull;

/**
 * The implementation of Realtime GLSL Editor.
 *
 * @author JavaSaBr
 */
public class RealTimeGLSLFileEditor extends AbstractFileEditor<HBox> {

    /**
     * The constant DESCRIPTION.
     */
    @NotNull
    public static final EditorDescription DESCRIPTION = new EditorDescription();

    static {
        DESCRIPTION.setConstructor(RealTimeGLSLFileEditor::new);
        DESCRIPTION.setEditorName("Realtime GLSL Editor");
        DESCRIPTION.setEditorId(RealTimeGLSLFileEditor.class.getSimpleName());
        DESCRIPTION.addExtension(FileExtensions.JME_MATERIAL);
    }

    @NotNull
    @Override
    protected HBox createRoot() {
        return new HBox();
    }

    @Override
    protected void createContent(@NotNull final HBox root) {

    }

    @NotNull
    @Override
    @FromAnyThread
    public EditorDescription getDescription() {
        return DESCRIPTION;
    }
}
